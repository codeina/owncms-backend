<?php

use Illuminate\Database\Seeder;

class BlogCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blog_categories')->insert([
            'name' => 'Programowanie',
            'slug' => 'programowanie'
        ]);
        DB::table('blog_categories')->insert([
            'name' => 'Hacking',
            'slug' => 'hacking'
        ]);
        DB::table('blog_categories')->insert([
            'name' => 'Linux',
            'slug' => 'linux'
        ]);
        DB::table('blog_categories')->insert([
            'name' => 'Tutoriale',
            'slug' => 'tutoriale'
        ]);

        // subcategories
        DB::table('blog_categories')->insert([
            'name' => 'Laravel',
            'slug' => 'php-laravel',
            'parent_id' => '1',
            'description' => 'Laravel - framework, który zdobywa coraz większą popularność w świecie PHP. Jedyny konkurent dla Symfony.',
        ]);
        DB::table('blog_categories')->insert([
            'name' => 'Java',
            'slug' => 'java',
            'parent_id' => '1',
            'description' => 'Najpopularniejszy język programowania, cieszący się największą ilością ofert pracy. Dla mnie to kolejny level programowania.',
        ]);


        DB::table('blog_categories')->insert([
            'name' => 'Python',
            'slug' => 'python',
            'parent_id' => '1',
            'description' => 'Python - dla mnie second language. Wykorzystuje go w automatyzacji procesów linuxowych, w przyszłości SI oraz machine learning.',
        ]);
        DB::table('blog_categories')->insert([
            'name' => 'Bezpieczeństwo',
            'slug' => 'bezpieczenstwo',
            'parent_id' => '2',
            'description' => 'Ważna kwestia w developmencie oprogramowania w dzisiejszych czasach. Napisać aplikację a napisać bezpieczną aplikację - to dwie różne kwestie.',
        ]);
        DB::table('blog_categories')->insert([
            'name' => 'Narzędzia',
            'slug' => 'narzedzia',
            'parent_id' => '2',
            'description' => 'Narzędzia pomagające wykonywanie testów bezpieczeństwa systemów, aplikacji, stron, usług.',
        ]);
        DB::table('blog_categories')->insert([
            'name' => 'Arch linux',
            'slug' => 'arch-linux',
            'parent_id' => '3',
            'description' => 'Zagadnienia, porady związane z Archem - dystrybucją dla zaawansowanych użytkowników.',
        ]);
        DB::table('blog_categories')->insert([
            'name' => 'Debian linux',
            'slug' => 'debian-linux',
            'parent_id' => '3',
            'description' => 'Zagadnienia, porady związane z Debianem - najpopularniejszą bazową dystrybucją Linuxa. Znana m.in. z Ubuntu, Minta.',
        ]);
        DB::table('blog_categories')->insert([
            'name' => '[Arch linux] Instalacja & konfiguracja',
            'slug' => 'instalacja-i-konfiguracja-arch-linux',
            'parent_id' => '4',
            'description' => 'Tutorial w którym omówimy krok po kroku instalację i konfigurację Archa.',
        ]);
        DB::table('blog_categories')->insert([
            'name' => '[Debian linux] Instalacja & konfiguracja',
            'slug' => 'instalacja-i-konfiguracja-debian-linux',
            'parent_id' => '4',
            'description' => 'Tutorial w którym omówimy krok po kroku instalację i konfigurację Debiana',
        ]);

    }
}
