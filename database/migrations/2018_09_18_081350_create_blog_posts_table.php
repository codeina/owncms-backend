<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')
                ->unique();
            $table->text('description', 500)
                ->nullable();
            $table->longText('body');
            $table->integer('category_id')
                ->nullable()
                ->unsigned();    
            $table->string('image')
                ->nullable();
            $table->boolean('status')
                ->default(0);
            $table->boolean('recommended')
                ->default(0);
            $table->timestamps();


            $table->foreign('category_id')
                ->references('id')
                ->on('blog_categories')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_posts');
    }
}
