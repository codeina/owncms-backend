<?php

namespace App\Contracts\Orders;

/**
 *
 * @author daniel
 */
interface OrderRepositoryInterface
{
    public function getAll();
    public function getLatest();
    public function findById($id);
    public function create(array $data);
    public function remove($id);
    public function update(array $data, $id, $attribute="id");
}