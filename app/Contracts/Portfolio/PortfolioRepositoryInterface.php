<?php


namespace App\Contracts\Portfolio;


interface PortfolioRepositoryInterface
{
    public function getAll();
    public function findById($id);
    public function create(array $data);
    public function remove($id);
    public function update(array $data, $id, $attribute="id");
}
