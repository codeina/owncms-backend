<?php


namespace App\Contracts\Blog;

/**
 *
 * @author daniel
 */
interface CategoryRepositoryInterface
{
    public function getAll();
    public function findById($id);
    public function findBySlug($slug);
    public function create(array $data);
    public function remove($id);
    public function update(array $data, $id, $attribute="id");
}
