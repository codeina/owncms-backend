<?php
/**
 * Created by PhpStorm.
 * User: s0lo
 * Date: 02.10.18
 * Time: 23:08
 */

namespace App\Services;

use Intervention\Image\Facades\Image;

class BlogService
{
    /**
     * @param $data
     * @return mixed
     */
    public function saveImage($data)
    {
        $image = $data;
        $filename = time() . '.' . $data->getClientOriginalExtension();
        $location = public_path('img/blog/' . $filename);
        Image::make($image)->save($location);

        return $filename;
    }

}