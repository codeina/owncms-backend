<?php

namespace App\Models\Users\Acl;
use Laratrust\Models\LaratrustPermission;
use Illuminate\Database\Eloquent\Model;

class Permission extends LaratrustPermission
{
    protected $table = 'permissions';

    protected $fillable = [
        'name',
        'display_name',
        'description'
    ];
}
