<?php

namespace App\Models\Blog;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function getRoutKeyName()
    {
        return 'slug';
    }

    protected $table = 'blog_posts';

    protected $fillable = [
        'name',
        'slug',
        'description',
        'image',
        'body',
        'category_id',
        'status',
        'recommended'
    ];

    /** Relations **/
    public function category()
    {
        return $this->belongsTo('App\Models\Blog\Category');
    }

}
