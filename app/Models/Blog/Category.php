<?php

namespace App\Models\Blog;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function getRouteKeyName()
    {
        return 'slug';
    }

    protected $table = 'blog_categories';

    protected $fillable = [
        'name',
        'slug',
        'description',
        'parent_id'
    ];


    /** Relations **/
    public function posts()
    {
        return $this->hasMany('App\Models\Blog\Post', 'category_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Blog\Category', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Blog\Category', 'parent_id');
    }
}
