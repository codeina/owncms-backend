<?php

namespace App\Models\Orders;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Order extends Model
{
    protected $table = 'orders';

    protected $fillable = [
        'order_nr',
        'client_name',
        'client_email',
        'product',
        'description'
    ];

    public function setOrderNr()
    {
        $latestOrder = Order::orderBy('created_at', 'DESC')->first();
        $this->order_nr = date("Y") . '/' . str_pad($latestOrder->id + 1, 4, "0", STR_PAD_LEFT);
        $this->save();
    }
}
