<?php

namespace App\Models\Portfolio;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    protected $table = 'portfolios';

    protected $fillable = [
        'name',
        'slug',
        'url',
        'description',
        'body'
    ];
}
