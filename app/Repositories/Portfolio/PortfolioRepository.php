<?php

namespace App\Repositories\Portfolio;


use App\Models\Portfolio\Portfolio;

class PortfolioRepository implements \App\Contracts\Portfolio\PortfolioRepositoryInterface
{
    /**
     * @var Category
     */
    private $model;

    /**
     * PortfolioRepository constructor.
     * @param Portfolio $model
     */
    public function __construct(Portfolio $model)
    {
        $this->model = $model;
    }

    /**
     * @return Portfolio[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        return $this->model::all();
    }

    public function findById($id)
    {
        return $this->model->findOrFail($id);
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function remove($id)
    {
        return $this->model->destroy($id);
    }

    public function update(array $data, $id, $attribute = "id")
    {
        return $this->model->where($attribute, '=', $id)->update($data);
    }
}
