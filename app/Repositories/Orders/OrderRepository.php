<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Repositories\Orders;

/**
 *
 * @author daniel
 */
class OrderRepository implements \App\Contracts\Orders\OrderRepositoryInterface
{


    public function model()
    {
        return \App\Models\Orders\Order::class;
    }

    public function getAll()
    {
        return $this->model()::all();
    }

    public function getLatest()
    {
        return $this->model()::get()->sortBy('created_at');
    }


    public function findById($id)
    {
        return $this->model()::findOrFail($id);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->model()::create($data)->setOrderNr();
    }

    public function remove($id)
    {
        return $this->model()::destroy($id);
    }

    public function update(array $data, $id, $attribute="id")
    {
        return $this->model()::where($attribute, '=', $id)->update($data);
    }
}
