<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Repositories\Blog;

/**
 *
 * @author daniel
 */
class PostRepository implements \App\Contracts\Blog\PostRepositoryInterface
{


    public function model()
    {
        return \App\Models\Blog\Post::class;
    }

    public function getAll()
    {
        return $this->model()::all();
    }

    public function getLatest($ile)
    {
        return $this->model()::get()->take($ile)->sortBy('created_at');
    }

    public function getRecomennded($ile)
    {
        return $this->model()::where('recommended', 1)->get()->take($ile)->sortBy('created_at');
    }


    public function findById($id)
    {
        return $this->model()::findOrFail($id);
    }
    public function findBySlug($slug)
    {
        return $this->model()::where('slug', $slug)
            ->firstOrFail();
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->model()::create($data);
    }

    public function remove($id)
    {
        return $this->model()::destroy($id);
    }

    public function update(array $data, $id, $attribute="id")
    {
        return $this->model()::where($attribute, '=', $id)->update($data);
    }


}
