<?php

namespace App\Http\Controllers\Frontend\Newsletter;

use App\Models\Newsletter\Newsletter;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Mail\newsletter as NewsletterEmail;
use App\Http\Requests\newsletter\NewsletterRequest;

class NewsletterFrontController extends Controller
{
    public function store(NewsletterRequest $request)
    {

        $newsletter = new Newsletter();
        $newsletter->fill($request->all());
        $newsletter->save();

        Mail::to($request->email)
            ->send(new NewsletterEmail());

        return redirect()->route('main.index');
    }
}
