<?php

namespace App\Http\Controllers\Frontend\Orders;

use App\Models\Orders\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\Orders\OrderRepositoryInterface as Repository;

class OrdersController extends Controller
{
    /**
     * Injection repository in constructor
     *
     * @var Repository
     *
     */
    private $repository;

    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function newOrder(Request $request)
    {
        $this->repository->create($request->all());


        return redirect()->route('main.index');
    }
}
