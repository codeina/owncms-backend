<?php

namespace App\Http\Controllers\Frontend\Blog;

use App\Http\Controllers\Controller;

use App\Contracts\Blog\PostRepositoryInterface as Repository;
use App\Contracts\Blog\CategoryRepositoryInterface as CategoryRepository;

class PostsController extends Controller
{
    /**
     * Injection repository in constructor
     *
     * @var Repository
     *
     */
    private $repository;
    private $categoryRepository;

    public function __construct(Repository $repository, CategoryRepository $categoryRepository)
    {
        $this->repository = $repository;
        $this->categoryRepository = $categoryRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllPosts()
    {
        $posts = $this->repository->getAll()->sortByDesc('name');
        $categories = $this->categoryRepository->getAll();
        return view('blog.posts', compact('posts', 'categories'));
    }

    /**
     * Display a listing of the latest posts.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLatestPosts()
    {
        $posts = $this->repository->getLatest();
        $categories = $this->categoryRepository->getAll();
        return view('blog.posts', compact('posts', 'categories'));
    }

    /**
     * Display a single post.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function getPost($slug)
    {
        $post = $this->repository->findBySlug($slug);
        $categories = $this->categoryRepository->getAll();
        return view('blog.post', compact('post', 'categories'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPostFromCategory($slug)
    {
        $cat = $this->categoryRepository->findBySlug($slug);
        $posts = $this->categoryRepository->findBySlug($slug)->posts;
        $categories = $this->categoryRepository->getAll();

        return view('blog.posts', compact('cat', 'posts', 'categories'));
    }


}
