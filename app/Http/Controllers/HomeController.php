<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts\Blog\PostRepositoryInterface as Repository;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    private $repository;

    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
//        $this->middleware('auth');
    }

    public function home()
    {
        $latestPosts = $this->repository->getLatest(3);
        $highRating = $this->repository->findById(1);
        $recommendedPosts = $this->repository->getRecomennded(3);
        return view('layouts.main', compact('latestPosts', 'highRating', 'recommendedPosts'));

    }
}
