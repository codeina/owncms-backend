<?php

namespace App\Http\Controllers\Admin\Users\Acl;

use App\Models\Users\Acl\Role;
use App\Models\Users\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RolesController extends Controller
{
    public function index()
    {
        $roles = Role::all();

        return view('dashboard.users.acl.roles.index', compact('roles'));
    }

    public function show($id)
    {
        $role = Role::findOrFail($id);

        return view('dashboard.users.acl.roles.show', compact('role'));
    }

    public function store(Request $request)
    {
        $role = new User($request->all());

        $role->fill($request->all());
        $role->save();

        return redirect()->route('roles.index')->with('success', 'Pomyślnie utworzono role');
    }

    public function update(Request $request, $id)
    {
        $role = Role::findOrFail($id);

        $role->fill($request->all());
        $role->save();

        return redirect()->route('roles.index')->with('success', 'Pomyślnie zaktualizowano role');
    }

    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role->delete();


        return redirect()->route('roles.index')->with('success', 'Pomyślnie usunięto role');
    }
}
