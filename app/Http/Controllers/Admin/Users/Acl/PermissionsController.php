<?php

namespace App\Http\Controllers\Admin\Users\Acl;


use App\Models\Users\Acl\Permission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PermissionsController extends Controller
{
    public function index()
    {
        $permissions = Permission::all();

        return view('dashboard.users.acl.permissions.index', compact('permissions'));
    }

    public function show($id)
    {
        $permission = Permission::findOrFail($id);

        return view('dashboard.users.acl.permissions.show', compact('permission'));
    }

    public function store(Request $request)
    {
        $permission = new Permission($request->all());

        $permission->fill($request->all());
        $permission->save();

        return redirect()->route('permissions.index')->with('success', 'Pomyślnie utworzono uprawnienia');
    }

    public function update(Request $request, $id)
    {
        $permission = Permission::findOrFail($id);

        $permission->fill($request->all());
        $permission->save();

        return redirect()->route('permissions.index')->with('success', 'Pomyślnie zaktualizowano uprawnienia');
    }

    public function destroy($id)
    {
        $permission = Permission::findOrFail($id);
        $permission->delete();


        return redirect()->route('permissions.index')->with('success', 'Pomyślnie usunięto uprawnienia');
    }
}
