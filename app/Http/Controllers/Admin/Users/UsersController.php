<?php

namespace App\Http\Controllers\Admin\Users;

use App\Models\Users\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::all();

        return view('dashboard.users.index', compact('users'));
    }

    public function show($id)
    {
        $user = User::findOrFail($id);

        return view('dashboard.users.show', compact('user'));
    }

    public function store(Request $request)
    {
        $user = new User($request->all());

        $user->fill($request->all());
        $user->save();

        return redirect()->route('users.index')->with('success', 'Pomyślnie utworzono użytkownika');
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $user->fill($request->all());
        $user->save();

        return redirect()->route('users.index')->with('success', 'Pomyślnie zaktualizowano użytkownika');
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();


        return redirect()->route('users.index')->with('success', 'Pomyślnie usunięto użytkownika');
    }
}
