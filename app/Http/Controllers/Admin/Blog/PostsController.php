<?php

namespace App\Http\Controllers\Admin\Blog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

use App\Contracts\Blog\PostRepositoryInterface as Repository;
use App\Contracts\Blog\CategoryRepositoryInterface as CategoryRepository;

use App\Http\Requests\blog\posts\CreateRequest;

class PostsController extends Controller
{
    /**
     * Injection repository in constructor
     *
     * @var Repository
     *
     */
    private $repository;
    private $categoryRepository;

    public function __construct(Repository $repository, CategoryRepository $categoryRepository)
    {
        $this->repository = $repository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = $this->repository->getAll();

        return view('dashboard.blog.posts.index', compact('posts'));
    }


    /**
     * Show view to create new post
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = $this->categoryRepository->getAll();

        return view('dashboard.blog.posts.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateRequest $request
     * @return string
     * @throws \Exception
     */
    public function store(CreateRequest $request)
    {

        $post = $this->repository->create($request->all());

        if($request->hasFile('image'))
        {
            $image = $request->file('image');
            $filename = time() . '.' . $request->file('image')->getClientOriginalExtension();
            $location = public_path('img/blog/' . $filename);
            Image::make($image)->save($location);
            $post->image = $filename;
        }

        $post->save();

        return redirect()->route('posts.index')->with('success', 'Udało się! Dodałes nowy post!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = $this->repository->findById($id);
        $categories = $this->categoryRepository->getAll();
        return view('dashboard.blog.posts.show', compact('post', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->repository->update($request->all('name', 'slug','description', 'image'), $id);
        $post = $this->repository->findById($id);
        $oldFilename = $post->image;
        $post->fill($request->all());

        if($request->hasFile('image')){
            //add the new photo
            $image = $request->file('image');
            $filename = time() . '.' . $request->file('image')->getClientOriginalExtension();
            $location = public_path('img/blog/' . $filename);
            Image::make($image)->save($location);

            try{
                unlink(public_path('img/blog/'.$oldFilename));
            }catch (\Exception $e) {

            }
            $post->image = $filename;
        }

        $post->save();

        return redirect()->route('posts.index')->with('success', 'Udało się! Zaktualizowaliśmy post poprawnie');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->remove($id);

        return redirect()->route('posts.index')->with('success', 'Udało się! Usunięto post poprawnie');
    }
}
