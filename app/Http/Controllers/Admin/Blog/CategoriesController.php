<?php

namespace App\Http\Controllers\Admin\Blog;

use App\Http\Controllers\Controller;

use App\Http\Requests\blog\categories\CreateRequest;
use App\Http\Requests\blog\categories\UpdateRequest;
use App\Contracts\Blog\CategoryRepositoryInterface as Repository;

class CategoriesController extends Controller
{

    /**
     * Injection repository in constructor
     *
     * @var Repository
     *
     */
    private $repository;

    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = $this->repository->getAll();

        return view('dashboard.blog.categories.index', compact('categories'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = $this->repository->getAll();
        return view('dashboard.blog.categories.create', compact('categories'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        $this->repository->create($request->all());

        return redirect()->route('categories.index')->with('success', 'Udało się! Dodano nową kategorię.');

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categories = $this->repository->getAll();
        $category = $this->repository->findById($id);
        $subcategories = $this->repository->findById($id)->children;
        $posts = $this->repository->findById($id)->posts;

        return view('dashboard.blog.categories.show', compact('category', 'posts', 'subcategories', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $this->repository->update($request->all('name', 'slug','description','parent_id'), $id);

        return redirect()->route('categories.index')->with('success', 'Udało się! Zaktualizowałeś kategorię');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->remove($id);

        return redirect()->route('categories.index')->with('success', 'Udało się! Usunięto kategorię!');

    }
}
