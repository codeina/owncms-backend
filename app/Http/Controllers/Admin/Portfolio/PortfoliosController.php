<?php

namespace App\Http\Controllers\Admin\Portfolio;


use App\Contracts\Portfolio\PortfolioRepositoryInterface as Repository;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

use App\Http\Requests\portfolio\CreateRequest;
use App\Http\Requests\portfolio\UpdateRequest;

class PortfoliosController extends Controller
{
    /**
     * Injection repository in constructor
     *
     * @var Repository
     *
     */
    private $repository;

    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $portfolios = $this->repository->getAll();

        return view('dashboard.portfolio.index', compact('portfolios'));
    }


    /**
     * Show view to create new portfolio
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.portfolio.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateRequest $request
     * @return string
     * @throws \Exception
     */
    public function store(CreateRequest $request)
    {

        $portfolio = $this->repository->create($request->all());

//        if($request->hasFile('image'))
//        {
//            $image = $request->file('image');
//            $filename = time() . '.' . $request->file('image')->getClientOriginalExtension();
//            $location = public_path('img/portfolio/' . $filename);
//            Image::make($image)->save($location);
//            $portfolio->image = $filename;
//        }
//
//        $portfolio->save();

        return redirect()->route('portfolios.index')->with('success', 'Udało się! Dodałes nowy portfolio!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $portfolio = $this->repository->findById($id);
        return view('dashboard.portfolio.show', compact('portfolio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $this->repository->update($request->all('name', 'slug','description', 'image'), $id);
        $portfolio = $this->repository->findById($id);
        $oldFilename = $portfolio->image;
        $portfolio->fill($request->all());

        if($request->hasFile('image')){
            //add the new photo
            $image = $request->file('image');
            $filename = time() . '.' . $request->file('image')->getClientOriginalExtension();
            $location = public_path('img/portfolio/' . $filename);
            Image::make($image)->save($location);

            try{
                unlink(public_path('img/portfolio/'.$oldFilename));
            }catch (\Exception $e) {

            }
            $portfolio->image = $filename;
        }

        $portfolio->save();

        return redirect()->route('portfolios.index')->with('success', 'Udało się! Zaktualizowaliśmy portfolio poprawnie');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->remove($id);

        return redirect()->route('portfolios.index')->with('success', 'Udało się! Usunięto portfolio poprawnie');
    }
}
