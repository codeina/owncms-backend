<?php

namespace App\Http\Requests\blog\posts;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:5|max:50',
            'slug' => 'required|min:3|max:25',
            'description' => 'max:255',
            'image' => 'sometimes',
            'body' => 'required'
        ];
    }
}
