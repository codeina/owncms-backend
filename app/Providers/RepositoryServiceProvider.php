<?php

/**
 * Created by Daniel Śmigiela &copy; 2018
 * All rights reserved
 */

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider {

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            \App\Contracts\Blog\CategoryRepositoryInterface::class,
            \App\Repositories\Blog\CategoryRepository::class
        );
        $this->app->bind(
            \App\Contracts\Blog\PostRepositoryInterface::class,
            \App\Repositories\Blog\PostRepository::class
        );
        $this->app->bind(
            \App\Contracts\Portfolio\PortfolioRepositoryInterface::class,
            \App\Repositories\Portfolio\PortfolioRepository::class
        );
        $this->app->bind(
            \App\Contracts\Orders\OrderRepositoryInterface::class,
            \App\Repositories\Orders\OrderRepository::class
        );
    }
}