<?php

use Illuminate\Http\Request;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::get('/categories', [
//    'uses' => 'Api\Blog\CategoriesController@index',
//    'as' => 'categories.index'
//    ]);
//Route::get('/category/{slug}', [
//    'uses' => 'Api\Blog\CategoriesController@show',
//    'as' => 'categories.show'
//]);
//
//Route::get('/posts', [
//    'uses' => 'Api\Blog\PostsController@index',
//    'as' => 'posts.index'
//    ]);
//
//Route::get('/posts-latest', [
//    'uses' => 'Api\Blog\PostsController@getLatest',
//    'as' => 'posts.index-latest'
//]);
//Route::get('/post/{id}', [
//    'uses' => 'Api\Blog\PostsController@show',
//    'as' => 'posts.show'
//]);
