<?php


Route::prefix('admin')->middleware('auth', 'web')->group(function() {
    Route::view('', 'dashboard.pages.dashboard')->name('dashboard.index');

    /** blog */
    Route::prefix('blog')->namespace('Admin\Blog')->group(function() {
        Route::view('', 'dashboard.pages.blog')->name('admin-blog.index');
        /** Kategorie */
        Route::get('categories', ['uses' => 'CategoriesController@index', 'as' => 'categories.index']);
        Route::get('category/{id}', ['uses' => 'CategoriesController@show', 'as' => 'categories.show']);
        Route::get('/categories/create', ['uses' => 'CategoriesController@create', 'as' => 'categories.create']);
        Route::delete('category/{id}', ['uses' => 'CategoriesController@destroy', 'as' => 'categories.delete']);
        Route::post('categories', ['uses' => 'CategoriesController@store', 'as' => 'categories.store']);
        Route::put('category/{id}', ['uses' => 'CategoriesController@update', 'as' => 'categories.update']);

        /** posty */
        Route::get('posts', ['uses' => 'PostsController@index', 'as' => 'posts.index']);
        Route::get('post/{id}', ['uses' => 'PostsController@show', 'as' => 'posts.show']);
        Route::get('posts/create', ['uses' => 'PostsController@create', 'as' => 'posts.create']);
        Route::post('posts', ['uses' => 'PostsController@store', 'as' => 'posts.store']);
        Route::put('post/{id}', ['uses' => 'PostsController@update', 'as' => 'posts.update']);
        Route::delete('{id}', ['uses' => 'PostsController@destroy', 'as' => 'posts.delete']);
    });
    Route::prefix('portfolio')->namespace('Admin\Portfolio')->group(function() {
        Route::get('', ['uses' => 'PortfoliosController@index', 'as' => 'portfolios.index']);
        Route::get('{id}', ['uses' => 'PortfoliosController@show', 'as' => 'portfolios.show']);
        Route::get('new/create', ['uses' => 'PortfoliosController@create', 'as' => 'portfolios.create']);
        Route::delete('{id}', ['uses' => 'PortfoliosController@destroy', 'as' => 'portfolios.delete']);
        Route::post('', ['uses' => 'PortfoliosController@store', 'as' => 'portfolios.store']);
        Route::put('{id}', ['uses' => 'PortfoliosController@update', 'as' => 'portfolios.update']);
    });
    Route::prefix('users')->namespace('Admin\Users')->group(function() {
        Route::get('', ['uses' => 'UsersController@index', 'as' => 'users.index']);
        Route::get('{id}', ['uses' => 'UsersController@show', 'as' => 'users.show']);
        Route::get('new/user', ['uses' => 'UsersController@create', 'as' => 'users.create']);
        Route::delete('{id}', ['uses' => 'UsersController@destroy', 'as' => 'users.delete']);
        Route::post('', ['uses' => 'UsersController@store', 'as' => 'users.store']);
        Route::put('user/{id}', ['uses' => 'UsersController@update', 'as' => 'users.update']);
    });
    Route::prefix('roles')->namespace('Admin\Users\Acl')->group(function() {
        Route::get('', ['uses' => 'RolesController@index', 'as' => 'roles.index']);
        Route::get('{id}', ['uses' => 'RolesController@show', 'as' => 'roles.show']);
        Route::get('new/create', ['uses' => 'RolesController@create', 'as' => 'roles.create']);
        Route::delete('{id}', ['uses' => 'RolesController@destroy', 'as' => 'roles.delete']);
        Route::post('', ['uses' => 'RolesController@store', 'as' => 'roles.store']);
        Route::put('{id}', ['uses' => 'RolesController@update', 'as' => 'roles.update']);
    });
    Route::prefix('permissions')->namespace('Admin\Users\Acl')->group(function() {
        Route::get('', ['uses' => 'PermissionsController@index', 'as' => 'permissions.index']);
        Route::get('{id}', ['uses' => 'PermissionsController@show', 'as' => 'permissions.show']);
        Route::get('new/create', ['uses' => 'PermissionsController@create', 'as' => 'permissions.create']);
        Route::delete('{id}', ['uses' => 'PermissionsController@destroy', 'as' => 'permissions.delete']);
        Route::post('', ['uses' => 'PermissionsController@store', 'as' => 'permissions.store']);
        Route::put('{id}', ['uses' => 'PermissionsController@update', 'as' => 'permissions.update']);
    });
});

Route::namespace('Frontend\Blog')->group(function() {
    Route::get('blog', ['uses' => 'PostsController@getAllPosts', 'as' => 'blog.index']);
    Route::get('blog/kategoria/{slug}', ['uses' => 'PostsController@getPostFromCategory', 'as' => 'blog.category']);
    Route::get('blog/post/{slug}', ['uses' => 'PostsController@getPost', 'as' => 'blog.singlePost']);
});

//Route::post('order/neworder', ['uses' => '\App\Http\Controllers\Frontend\Orders\OrdersController@newOrder', 'as' => 'order.newOrder']);


Route::get('', ['uses' => 'HomeController@home', 'as' => 'main.index']);
Route::post('newsletter', ['uses' => '\App\Http\Controllers\Frontend\Newsletter\NewsletterFrontController@store', 'as' => 'newsletter.store']);
Auth::routes();

//Route::view('/blog', 'layouts.blog');

Route::get('/home', 'HomeController@index')->name('home');
