@extends('layouts.blog')
@section('posts')
    @foreach($posts->chunk(4) as $items)
        <div class="row m-t-50">
            @foreach($items as $post)
                <a href="{{route('blog.singlePost', $post->slug)}}" class="card-link">
                    <div class="post-card card-hover" style="width: 18rem;">
                        <img class="post-card-image" src="{{asset('img/blog/'.$post->image)}}" alt="{!! $post->slug !!}">
                        <div class="post-card-body">
                            <h5 class="post-card-header text-orange">{!! $post->name !!}</h5>
                            <p class="post-card-content">{!!str_limit(strip_tags($post->description), $limit=120)!!}</p>
                        </div>
                    </div>
                </a>
            @endforeach
        </div>
    @endforeach
@endsection