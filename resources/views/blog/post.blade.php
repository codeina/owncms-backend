@extends('layouts.blog')
@section('posts')
<div class="container m-t-50">
<div class="row">
    <div class="col-lg-8">
        <div class="article">
            <img style="max-width: 100%;" src="{{asset('/img/blog/'.$post->image)}}" alt="">
            <p>{!! $post->description !!}</p>
            <p>{!! $post->body !!}</p>
        </div>
    </div>
</div>
</div>
@endsection