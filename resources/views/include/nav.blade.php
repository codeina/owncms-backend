<nav class="navbar navbar-custom navbar-expand-lg navbar-light bg-transparent" style="box-shadow: none;">
    <a class="navbar-brand logo" href="{{route('main.index')}}">Smigiela.pl</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link  custom-nav-link" href="#about">O mnie</a>
            </li>
            <li class="nav-item">
                <a class="nav-link custom-nav-link" href="{{route('blog.index')}}">Blog</a>
            </li>
            {{--<li class="nav-item">--}}
            {{--<a class="nav-link custom-nav-link" href="#portfolio">Realizacje </a>--}}
            {{--</li>--}}
            {{--<li class="nav-item">--}}
            {{--<a class="nav-link custom-nav-link" href="#">Oferta</a>--}}
            {{--</li>--}}
            <li class="nav-item">
                <a class="nav-link custom-nav-link" href="#">Kontakt</a>
            </li>
        </ul>
    </div>
</nav>