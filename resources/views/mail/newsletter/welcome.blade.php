<!DOCTYPE html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Niramit" rel="stylesheet">
    <meta charset="UTF-8">
</head>

<style>
    .header{
        padding: 25px 50px 25px 50px;
        background-color: #00d4ff;
        color: white;
        text-align: center;
    }
    .body{
        padding:100px;
        color: black;
        font-family: 'Niramit', sans-serif;
        border-left: black 2px solid;
        border-right: black 2px solid;
    }
    .footer{
        margin: 25px;
        padding: 25px 50px 25px 50px;
        background-image: radial-gradient(circle, #6383b7, #416991, #26506c, #103749, #031f29);
        color: white;
    }
    .footer p{
        font-size: 11px;
    }
    .footer a{text-decoration: underline; color: inherit;}
    .footer a:hover{color: inherit;}

    .helion img{max-width: 260px;}
</style>


<body>
<div class="container">
    <div class="header" >
        <h1>Cześć!</h1>
        <h4>Dziękuję za zaufanie!</h4>
    </div>
    <div class="body">
        <h2>Twój email został zapisany w mojej bazie.</h2>
        <p>Zostanie wykorzystany tylko i wyłącznie w celu informowania Cię o nowościach na blogu</p>
        <hr>
        <div class="helion">
            <div class="row">
                <div class="col-lg-8">
                    <p>W podzięce za zaufanie otrzymujesz ode mnie link do ciekawego ebooka do pobrania z renomowanej księgarni zupełnie za darmo: [kliknij okładkę]:</p>
                    <hr>
                    <p style="font-style: italic;">W obecnych warunkach intensywnego rozwoju nowoczesnych technologii funkcjonowanie przedsiębiorstw zyskało nowe cechy i możliwości.
                        Technologie obliczeniowe i elektronicznej wymiany informacji wielorako wpływają na sposoby działania firm. Z jednej strony stanowią narzędzia pomocne do usprawniania pracy przedsiębiorstw, z drugiej zaś mogą być podstawą do tworzenia nowych przedsięwzięć biznesowych.
                        Wnioski zawarte w monografii ukazują konieczność pogłębiania wiedzy na temat zastosowania nowych technologii w zarządzaniu przedsiębiorstwem na wielu polach jego działalności. Uświadomienie sobie szans i zagrożeń, które pojawiają się wraz z nimi, zwiększa możliwości firm w walce konkurencyjnej.</p>
                </div>
                <div class="col-lg-4" style="text-align: center">
                    <a href="https://helion.pl/view/90402/ksiazki/funkcjonowanie-e-biznesu-zasoby-procesy-technologie-maria-czajkowska-maciej-malarski,e_05vn.htm#format/e" target="_blank">
                        <img src="https://static01.helion.com.pl/global/okladki/326x466/a8d678a2178712855cedfacea3c60477,e_05vn.jpg" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer">
        <p>Administratorem Twoich danych jest Daniel Śmigiela z portalu smigiela.pl</p>
        <p>Aby zrezygnować z otrzymywania informacji na podany adres email, możesz się wypisać <a href="unsubscribe">tutaj</a></p>
    </footer>
</div>
</body>
