@extends('layouts.dashboard')
@section('content')
    <div class="jumbotron jumbotron-fluid header-jumbotron">
        <div class="container m-b-100">
            <h1 class="display-4">Blog</h1>
            <p class="lead">Zarządzaj swoimi postami, kategoriami. Od dziś to takie proste.</p>
        </div>
        <hr class="">
        <div class="container text-center" style="margin: 0 auto;">
            <h3>Co chcesz zrobić?</h3>
            <div class="row justify-content-center">
                <div class="col-lg-4">
                    <a class="btn btn-lg btn-outline btn-in-jumbotron" href="{{route('posts.index')}}">Idź do postów</a>
                </div>
                <div class="col-lg-4">
                    <a class="btn btn-lg btn-outline btn-in-jumbotron" href="{{route('categories.index')}}">Idź do kategorii</a>
                </div>
            </div>
        </div>
    </div>
@endsection