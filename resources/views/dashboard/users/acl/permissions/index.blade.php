@extends('layouts.dashboard')
@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="lg-8">
                    <h2 class="title">Uprawnienia użytkowników</h2>
                </div>
                <div class="col-lg-2 ml-auto">
                    <a class="btn btn-outline-info" type="button" href="{{route('permissions.create')}}">Dodaj</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nazwa:</th>
                    <th scope="col">Nazwa wyświetlana:</th>
                    <th scope="col">Akcje:</th>
                </tr>
                </thead>
                <tbody>
                @foreach($permissions as $permission)
                    <tr>
                        <td>{{$permission->id}}</td>
                        <td><a class="linkToObject" href="{{route('permissions.show', $permission->id)}}">{{$permission->name}}</a></td>
                        <td>{{$permission->display_name}}</td>
                        <td>
                            {!! Form::open(['route' => ['permissions.delete', $permission->id], 'method' => 'DELETE']) !!}
                            <button class="btn btn-sm rounded btn-outline-danger">X</button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
