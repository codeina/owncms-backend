@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="card-header">
            <div class="row">
                <div class="lg-6">
                    <h2 class="title">{!! $role->display_name !!}</h2>
                </div>
                <div class="col-lg-4 ml-auto">
                    <div class="row">
                        <div class="col-lg-4">
                            <button class="btn btn-outline-info" type="button" name="change_disable"
                                    id="change_disable">Edytuj
                            </button>
                        </div>
                        <div class="col-lg-4">
                            {!! Form::open(['route' => ['roles.delete', $role->id], 'method' => 'DELETE']) !!}
                            <button class="btn btn-outline-danger">Usuń</button>
                            {!! Form::close() !!}
                        </div>
                    </div>


                </div>
            </div>

        </div>
        <div class="card-body">
            {!! Form::model($role, ['route' => ['roles.update', $role->id]]) !!}
            <input name="_method" type="hidden" value="PUT">
            <div class="form-group row">
                <div class="col-lg-2">
                    {!! Form::label('name', 'Nazwa:', ['class' => 'label']) !!}
                </div>
                <div class="col-lg-10">
                    {!! Form::text('name', null, ['class' => 'form-control inputDisabled', 'disabled']) !!}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-2">
                    {!! Form::label('display_name', 'Email:', ['class' => 'label']) !!}
                </div>
                <div class="col-lg-10">
                    {!! Form::text('display_name', null, ['class' => 'form-control inputDisabled', 'disabled']) !!}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-2">
                    {!! Form::label('description', 'Opis:', ['class' => 'label']) !!}
                </div>
                <div class="col-lg-10">
                    {!! Form::textarea('description', null, ['class' => 'form-control inputDisabled', 'rows' => '3', 'disabled']) !!}
                </div>
            </div>
            {!! Form::submit('Zapisz', ['class' => 'btn btn-outline-success m-t-5']) !!}
            {!! Form::close() !!}
        </div>
    </div>




    @push('scripts')
        <script> <!-- Remove disable arttribute from inputs and edit on -->
            $("#change_disable").click(function (event) {
                event.preventDefault();
                $('.inputDisabled').removeAttr("disabled")
                $('#change_disable').addClass("d-none")
            });
        </script>
    @endpush

@endsection
