@extends('layouts.dashboard')
@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="lg-8">
                    <h2 class="title">Użytkownicy aplikacji</h2>
                </div>
                <div class="col-lg-2 ml-auto">
                    <a class="btn btn-outline-info" type="button" href="{{route('users.create')}}">Dodaj</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nazwa:</th>
                    <th scope="col">Email:</th>
                    <th scope="col">Rola</th>
                    <th scope="col">Akcje:</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{$user->id}}</td>
                        <td><a class="linkToObject" href="{{route('users.show', $user->id)}}">{{$user->name}}</a></td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->role}}
                        </td>
                        <td>
                            {!! Form::open(['route' => ['users.delete', $user->id], 'method' => 'DELETE']) !!}
                            <button class="btn btn-sm rounded btn-outline-danger">X</button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
