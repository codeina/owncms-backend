@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="card-header">
            <h2 class="title">Dodaj nową kategorię</h2>
        </div>
        <div class="card-body">
            {!! Form::open(['route' => 'categories.store', 'method' => 'POST']) !!}
            <div class="form-group row">
                <div class="col-lg-2">
                    {!! Form::label('name', 'Nazwa:', ['class' => 'label']) !!}
                </div>
                <div class="col-lg-10">
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-2">
                    <div class="label">
                        Kategoria nadrzędna:
                    </div>
                </div>
                <div class="col-lg-10">
                    <select class="custom-select" name="parent_id">
                        @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-2">
                    <label for="slug">Przyjazny adres:<br>
                        <small>http://codeina.net/blog/kategorie/przyjazny-adres</small>
                    </label>
                </div>
                <div class="col-lg-10">
                    {!! Form::text('slug', null, ['class' => 'form-control']) !!}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-2">
                    {!! Form::label('description', 'Opis:', ['class' => 'label']) !!}
                </div>
                <div class="col-lg-10">
                    {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => '3']) !!}
                </div>
            </div>
            {!! Form::submit('Dodaj', ['class' => 'btn btn-outline-success m-t-5']) !!}
            {!! Form::close() !!}
        </div>
    </div>



@endsection
