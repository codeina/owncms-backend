@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="card-header">
            <div class="row">
                <div class="lg-6">
                    <h2 class="title">Zamówienie: {!! $order->order_nr !!}</h2>
                    <h3>Klient: {!! $order->client_name !!}</h3>
                </div>
                <div class="col-lg-4 ml-auto">
                    <div class="row">
                        <div class="col-lg-4">
                            <button class="btn btn-outline-info" type="button" name="change_disable"
                                    id="change_disable">Edytuj
                            </button>
                        </div>
                        <div class="col-lg-4">
                            {!! Form::open(['route' => ['orders.delete', $order->id], 'method' => 'DELETE']) !!}
                            <button class="btn btn-outline-danger">Usuń</button>
                            {!! Form::close() !!}
                        </div>
                    </div>


                </div>
            </div>

        </div>
        <div class="card-body">
            {!! Form::model($order, ['route' => ['orders.update', $order->id]]) !!}
            <input name="_method" type="hidden" value="PUT">
            <div class="form-group row">
                <div class="col-lg-2">
                    {!! Form::label('client_name', 'Nazwa klienta:', ['class' => 'label']) !!}
                </div>
                <div class="col-lg-10">
                    {!! Form::text('client_name', null, ['class' => 'form-control inputDisabled', 'disabled']) !!}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-2">
                    {!! Form::label('client_email', 'Email klienta:', ['class' => 'label']) !!}
                </div>
                <div class="col-lg-10">
                    {!! Form::text('client_email', null, ['class' => 'form-control inputDisabled', 'disabled']) !!}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-2">
                    <div class="label">
                        Produkt:
                    </div>
                </div>
                <div class="col-lg-10">
                    <select name="product" class="form-control">
                        <option value="starter">Starter</option>
                        <option value="cms">CMS</option>
                        <option value="wordpress">Wordpress</option>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-2">
                    {!! Form::label('description', 'Opis:', ['class' => 'label']) !!}
                </div>
                <div class="col-lg-10">
                    {!! Form::textarea('description', null, ['class' => 'form-control inputDisabled', 'rows' => '3', 'disabled']) !!}
                </div>
            </div>
            {!! Form::submit('Zapisz', ['class' => 'btn btn-outline-success m-t-5']) !!}
            {!! Form::close() !!}
        </div>
    </div>

    @if($subcategories->count() > 0)
        <div class="card-header">
            <h2 class="title">Podkategorie</h2>
        </div>
        <div class="card-body">
            @if($subcategories->count() > 0)
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Nazwa</th>
                        <th scope="col">Postów:</th>
                        <th scope="col">Usuń:</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($subcategories as $subcategory)
                        <tr>
                            <td>{{$subcategory->id}}</td>
                            <td><a class="linkToObject"
                                   href="{{route('categories.show', $subcategory->id)}}">{{$subcategory->name}}</a>
                            </td>
                            <td>{{$subcategory->Post->count()}}</td>
                            <td>
                                {!! Form::open(['route' => ['categories.delete', $subcategory->id], 'method' => 'DELETE']) !!}
                                <button class="btn btn-outline-danger">X</button>
                                {!! Form::close() !!}</td>
                        </tr>
                    @endforeach
                    @else <h3>Brak postów do wyświetlenia</h3>
                    @endif
                    </tbody>
                </table>
        </div>
    @else
        <div class="card m-t-20">
            <div class="card-header">
                <h2 class="title">Posty w kategorii</h2>
            </div>
            <div class="card-body">
                @if($posts->count() > 0)
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nazwa</th>
                            <th scope="col">Opis</th>
                            <th scope="col">Usuń</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($posts as $post)
                            <tr>
                                <td>{{$post->id}}</td>
                                <td><a class="linkToObject"
                                       href="{{route('posts.show', $post->id)}}">{{$post->name}}</a>
                                </td>
                                <td>{{$post->description}}
                                <td>
                                    <button class="btn btn-danger">Usuń</button>
                                </td>
                            </tr>
                        @endforeach
                        @else <h3>Brak postów do wyświetlenia</h3>
                        @endif
                        </tbody>
                    </table>
            </div>
        </div>
    @endif


    @push('scripts')
        <script> <!-- Remove disable arttribute from inputs and edit on -->
            $("#change_disable").click(function (event) {
                event.preventDefault();
                $('.inputDisabled').removeAttr("disabled")
                $('#change_disable').addClass("d-none")
            });
        </script>
    @endpush

@endsection
