@extends('layouts.dashboard')
@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="lg-8">
                    <h2 class="title">Zamówienia</h2>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Klient:</th>
                    <th scope="col">Email:</th>
                    <th scope="col">Zamówienie:</th>
                    <th scope="col">Akcje:</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
                    <tr>
                        <td><a class="linkToObject" href="{{route('orders.show', $order->id)}}">{{$order->order_nr}}</a></td>
                        <td>{{$order->client_name}}</td>
                        <td>{{$order->client_email}}</td>
                        <td>{!! $order->product !!}</td>
                        <td>
                            <button class="btn btn-sm btn-outline-danger">Zmień status</button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
