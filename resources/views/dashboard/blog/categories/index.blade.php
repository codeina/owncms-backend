@extends('layouts.dashboard')
@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="lg-8">
                    <h2 class="title">Kategorie postów na blogu</h2>
                </div>
                <div class="col-lg-2 ml-auto">
                    <a class="btn btn-outline-info" type="button" href="{{route('categories.create')}}">Dodaj</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nazwa:</th>
                    <th scope="col">Opis:</th>
                    <th scope="col">Kat. nadrzędna:</th>
                    <th scope="col">Akcje:</th>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $category)
                    <tr>
                        <td>{{$category->id}}</td>
                        <td><a class="linkToObject" href="{{route('categories.show', $category->id)}}">{{$category->name}}</a></td>
                        <td>{{$category->description}}</td>
                        <td>
                            @if($category->parent()->count() > 0)
                                <p>{{$category->parent->name}}</p>
                            @else
                                <p>-</p>
                            @endif
                        <td>
                            {!! Form::open(['route' => ['categories.delete', $category->id], 'method' => 'DELETE']) !!}
                            <button class="btn btn-sm rounded btn-outline-danger">X</button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
