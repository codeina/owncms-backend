@extends('layouts.dashboard')
@section('content')
<div class="card container-fluid">
    <div class="card-header">
        <h2 class="title">Dodaj nowy post</h2>
    </div>
    <div class="card-body">
        {!! Form::open(['route' => 'posts.store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
          <div class="form-group row">
            <div class="col-lg-2">
              <div class="label">
                Nazwa
              </div>
              </div>
              <div class="col-lg-10">
                <input class="form-control" type="text" name="name">
              </div>
          </div>

        <div class="form-group row">
            <div class="col-lg-2">
                <label for="slug">Przyjazny adres:<br><small>http://codeina.net/blog/posts/przyjazny-adres</small></label>
            </div>
            <div class="col-lg-10">
                {!! Form::text('slug', null, ['class' => 'form-control']) !!}
            </div>
        </div>

          <div class="form-group row">
            <div class="col-lg-2">
              <div class="label">
                Opis
              </div>
              </div>
              <div class="col-lg-10">
                <textarea class="form-control" type="text" name="description"></textarea>
              </div>
          </div>

          <div class="form-group row">
            <div class="col-lg-2">
              <div class="label">
                Treść
              </div>
              </div>
              <div class="col-lg-10">
                <textarea class="tinymce form-control" type="text" name="body" rows="8"></textarea>
              </div>
          </div>

          <div class="form-group row">
            <div class="col-lg-2">
              <div class="label">
                Zdjęcie
              </div>
              </div>
              <div class="col-lg-10">
                <input type="file" name="image" value="">
              </div>
          </div>

          <div class="form-group row">
            <div class="col-lg-2">
              <div class="label">
                Kategoria:
              </div>
              </div>
              <div class="col-lg-10">
                  <select class="custom-select" name="category_id">
                      @foreach($categories as $category)
                      <option value="{{$category->id}}">{{$category->name}}</option>
                      @endforeach
                  </select>
              </div>
          </div>

        <div class="form-group row">
            <div class="col-lg-2">
                <div class="label">
                    Status:
                </div>
            </div>
            <div class="col-lg-10">
                <select class="custom-select" name="status">
                        <option value="DRAFT">Szkic</option>
                        <option value="PUBLISHED">Opublikowane</option>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-lg-2">
                <div class="label">
                    Rekomendowane:
                </div>
            </div>
            <div class="col-lg-10">
                <select class="custom-select" name="recommended">
                    <option value=""></option>
                    <option value="0">Nie</option>
                    <option value="1">Tak</option>
                </select>
            </div>
        </div>

        {!! Form::submit('Dodaj', ['class' => 'btn btn-outline-success m-t-5']) !!}
        {!! Form::close() !!}
    </div>
</div>
@endsection

@push('scripts')
    <script>
        tinymce.init({
            selector: 'textarea.tinymce',
            plugins: 'image imagetools code link',
            height: '230'
        });
    </script>
@endpush