    @extends('layouts.dashboard')
    @section('content')
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="lg-8">
                        <h2 class="title">Posty:</h2>
                    </div>
                    <div class="col-lg-2 ml-auto">
                        <a class="btn btn-outline-info" type="button" href="{{route('posts.create')}}">Dodaj</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nazwa</th>
                        <th scope="col">Kategoria</th>
                        <th scope="col">Zdjęcie</th>
                        <th scope="col">Akcje</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($posts as $post)
                        <tr>
                            <td>{{$post->id}}</td>
                            <td><a class="linkToObject" href="{{route('posts.show', $post->id)}}">{{$post->name}}</a></td>
                            <td>
                                    <p>{{$post->category->name}}</p>
                            <td>{{$post->image}}</td>
                            <td>{!! Form::open(['route' => ['posts.delete', $post->id], 'method' => 'DELETE']) !!}
                                <button class="btn btn-sm rounded btn-outline-danger">X</button>
                                {!! Form::close() !!}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endsection
