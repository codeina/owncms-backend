@extends('layouts.dashboard')
@section('content')
    <div class="card container-fluid">
        <div class="card-header">
            <h2 class="title">Dodaj nową realizację</h2>
        </div>
        <div class="card-body">
            {!! Form::open(['route' => 'portfolios.store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
            <div class="form-group row">
                <div class="col-lg-2">
                    <div class="label">
                        Nazwa
                    </div>
                </div>
                <div class="col-lg-10">
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-2">
                    <div class="label">
                        URL
                    </div>
                </div>
                <div class="col-lg-10">
                    {!! Form::text('url', null, ['class' => 'form-control']) !!}
                </div>
            </div>


            <div class="form-group row">
                <div class="col-lg-2">
                    <label for="slug">Przyjazny adres:<br><small>http://codeina.net/blog/posts/przyjazny-adres</small></label>
                </div>
                <div class="col-lg-10">
                    {!! Form::text('slug', null, ['class' => 'form-control']) !!}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-2">
                    <div class="label">
                        Opis
                    </div>
                </div>
                <div class="col-lg-10">
                    <textarea class="form-control" type="text" name="description"></textarea>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-2">
                    <div class="label">
                        Treść
                    </div>
                </div>
                <div class="col-lg-10">
                    <textarea class="form-control tinymce" type="text" name="body"></textarea>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-2">
                    <div class="label">
                        Zdjęcie
                    </div>
                </div>
                <div class="col-lg-10">
                    <input type="file" name="image" value="">
                </div>
            </div>

            {!! Form::submit('Dodaj', ['class' => 'btn btn-outline-success m-t-5']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        tinymce.init({
            selector: 'textarea.tinymce',
            plugins: 'image imagetools code link',
            height: '230'
        });
    </script>
@endpush