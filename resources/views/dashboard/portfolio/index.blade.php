@extends('layouts.dashboard')
@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="lg-8">
                    <h2 class="title">Realizacje:</h2>
                </div>
                <div class="col-lg-2 ml-auto">
                    <a class="btn btn-outline-info" type="button" href="{{route('portfolios.create')}}">Dodaj</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nazwa</th>
                    <th scope="col">Link</th>
                    <th scope="col">Zdjęcie</th>
                    <th scope="col">Akcje</th>
                </tr>
                </thead>
                <tbody>
                @foreach($portfolios as $portfolio)
                    <tr>
                        <td>{{$portfolio->id}}</td>
                        <td><a class="linkToObject" href="{{route('portfolios.show', $portfolio->id)}}">{{$portfolio->name}}</a></td>
                        <td>{!! $portfolio->url !!}</td>
                        <td>{{$portfolio->image}}</td>
                        <td>{!! Form::open(['route' => ['portfolios.delete', $portfolio->id], 'method' => 'DELETE']) !!}
                            <button class="btn btn-sm rounded btn-outline-danger">X</button>
                            {!! Form::close() !!}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
