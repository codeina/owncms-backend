<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<section id="blog-intro">
    @include('.include/nav')
    <div class="intro-b">
        <div class="intro-text text-orange">
            @if(\Illuminate\Support\Facades\Request::is('blog/post/*'))
                <h1 class="text-uppercase">{!! $post->name !!}</h1>
            @elseif(\Illuminate\Support\Facades\Request::is('blog/kategoria/*'))
                <h1 class="text-uppercase">{!! $cat->name !!}</h1>
            @elseif(\Illuminate\Support\Facades\Request::is('blog'))
                <h1 class="text-uppercase">Blog</h1>
            @endif
        </div>
    </div>
</section>
<section class="p-l-20 p-r-20">
    <div class="row">
        <div class="col-md-10">
            @yield('posts')
        </div>
        <div class="col-md-2 p-t-50">
            <div class="text-orange">
                <p class="main-text">Kategorie:</p>
                <ul class="list-unstyled">

                    @foreach($categories as $cat)
                        @if($cat->parent_id ==0)
                            <li class="main-text cat-header">{!! $cat->name !!}:</li>
                        @endif
                        @foreach($cat->children as $child)
                            <ul class="m-l-10 list-unstyled">
                                <a style="color: black; " href="{{route('blog.category', $child->slug)}}"><li>{!! $child->name !!} ({!! $child->posts->count() !!})</li></a>
                            </ul>
                        @endforeach
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="footer bg-gradient">
    <p>
        Daniel Śmigiela &copy; 2018. Wszelkie prawa zastrzeżone.
    </p>
</section><!-- end footer -->
</body>
</html>