<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<style>

</style>
<body>
<div id="app">
    <section id="intro">
        @include('.include.nav')
        <div class="intro-b">
            <div class="intro-text">
                <h1 class="text-uppercase">Smigiela.pl - Blog informatyczny</h1>
                <h3 class="m-t-20">"Być częścią rozwiązania, nie częścią problemu"</h3>
                <a href="#about" class="m-t-50 btn btn-outline-orange">O mnie</a>
            </div>
        </div>
    </section>
    <section id="posts" class="container p-t-100 p-b-30">
        <div class="row">
            <div class="col-lg-8">
                    <div class="horizontal-card news-big bg-white">
                        <div class="row">
                            <div class="col-lg-7">
                                <img class="width-100"
                                     src="https://www.g-technology.com/content/dam/g-tech/en-us/assets/home/G-DRIVE%20mobile%20SSD%20R-Series_Video_Thumbnail.png.thumb.3000.3000.png"
                                     alt="{!! $highRating->slug !!}">
                            </div>
                            <div class="col-lg-4">
                                <div class="post-card-body">
                                    <div class="post-card-header text-orange" style="border-bottom: 1px black solid;">
                                        {!! $highRating->name !!}
                                    </div>
                                    <div class="post-card-content">
                                        {!! $highRating->description !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    @foreach($latestPosts as $post)
                    <div class="post-card-body post-card-small">
                        <img class="d-inline"
                             src="https://www.g-technology.com/content/dam/g-tech/en-us/assets/home/G-DRIVE%20mobile%20SSD%20R-Series_Video_Thumbnail.png.thumb.3000.3000.png"
                             style="max-width: 100px" alt="{!! $post->slug !!}">
                        <p class="text-orange d-inline">{!! $post->name !!}</p>
                    </div>
                    @endforeach
                </div>
        </div>
            <hr>
        <div class="text-center">Rekomendowane</div>
        <div class="row">
                @foreach($recommendedPosts as $rp)
                <div class="col-lg-4 col-sm-4 col-xs-6">
                    <div class="post-card bg-white">
                        <img src="https://www.g-technology.com/content/dam/g-tech/en-us/assets/home/G-DRIVE%20mobile%20SSD%20R-Series_Video_Thumbnail.png.thumb.3000.3000.png"
                             alt="" class="post-card-image">
                        <div class="post-card-body">
                            <div class="post-card-header text-orange">
                                {!! $rp->name !!}
                            </div>
                            <div class="post-card-content">
                                {!! $rp->description !!}
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        <div class="m-auto">
            <a href="{!! route('blog.index') !!}" class="m-t-50 btn btn-lg btn-block btn-outline-orange">Zobacz wszystkie posty</a>
        </div>
    </section>

    <section id="cytat" class="text-center bg-gradient-orange">
        <h1 class="text-white" style="font-style: italic">" Sięgaj po swoje marzenia! "</h1>
    </section>

    <section id="about" class="p-t-100 p-b-30">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h2 class="text-orange">Daniel Śmigiela</h2>
                    <p class="font-italic">Freelancer, przedsiębiorca</p>
                    <p>Z pasją tworzę kolejne aplikacje webowe i skutecznie rozwijam się w rozwiązaniach korporacyjnych.
                        W swojej pracy wykorzystuję głównie PHP(laravel), MySQL, VueJS, lecz uskuteczniam również inne
                        jezyki programowania: Java, Python.
                    </p>
                    <p>Pasjonuje mnie cyber bezpieczeństwo, architektury oraz wzorce projektowe. Stawiam na czysty kod,
                        wiedzę, którą chcę przekazywać na łamach tego bloga.</p>
                </div>

                <div class="col-lg-6">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="skillbar" data-percent="55%">
                                    <div class="skillbar-title">Web design</div>
                                    <div class="skill-bar-percent">55%</div>
                                    <div class="skillbar-bar" style="width: 55%;"></div>
                                </div>

                                <div class="skillbar" data-percent="80%">
                                    <div class="skillbar-title">development</div>
                                    <div class="skill-bar-percent">80%</div>
                                    <div class="skillbar-bar" style="width: 80%;"></div>
                                </div>

                                <div class="skillbar" data-percent="60%">
                                    <div class="skillbar-title">Backend</div>
                                    <div class="skill-bar-percent">60%</div>
                                    <div class="skillbar-bar" style="width: 60%;"></div>
                                </div>

                                <div class="skillbar" data-percent="70%">
                                    <div class="skillbar-title">branding</div>
                                    <div class="skill-bar-percent">70%</div>
                                    <div class="skillbar-bar" style="width: 70%;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <hr>
    <section id="newsletter" class=" p-t-100 is-relative" style="margin-bottom: 300px;">
        <div class="is-absolute" style="left: 20%; bottom: -280px;">
            <img src="{{asset('/img/main/responsive.png')}}" class="" style="max-width: 42%" alt="">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <div style="background-color: transparent; padding: 1.2rem" class="text-center">
                        <h1 class="title text-orange">Newsletter</h1>
                    </div>
                </div>
                <div class="col-lg-7">
                    {!! Form::open(['route' => 'newsletter.store', 'method' => 'POST']) !!}
                        <div class="newsletter">
                            <img src="{{asset('/img/main/newsletter.jpeg')}}" alt="">
                            <div class="field">
                                {!! Form::email('email', null, ['placeholder' => 'Zostaw swój email!']) !!}
                                <button class="custom-submit" type="submit"><i class="fa fa-3x fa-check-circle"></i>
                                </button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
    <section class="footer bg-gradient">
        <p>
            Daniel Śmigiela &copy; 2018. Wszelkie prawa zastrzeżone.
        </p>
    </section>
</div>

</body>
</html>