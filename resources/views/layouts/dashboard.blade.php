<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>CodeinaCMS</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>

<body>
    <div class="">
      <nav class="navbar navbar-expand-lg navbar-dark">
            <a class="navbar-brand" href="#">CodeinaCMS</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Profil</button>
                </form>
            </div>
      </nav>
        <div class="row">
            <div class="col asside">
              <div class="asside-content">
                <a class="menu-item-link" href="{{route('dashboard.index')}}"><p class="asside-menu-item">Dashboard</p></a>
                <ul class="asside-menu">
                  <a class="menu-item-link" href="{{route('admin-blog.index')}}"><li class="asside-menu-item">Blog</li></a>
                    <ul class="asside-submenu">
                      <a class="menu-item-link" href="{{route('posts.index')}}"><li class="asside-menu-item">Posty</li></a>
                      <a class="menu-item-link" href="{{route('categories.index')}}"><li class="asside-menu-item">Kategorie</li></a>
                    </ul>
                  </ul>
                  <a class="menu-item-link" href="{{route('portfolios.index')}}"><li class="asside-menu-item">Portfolio</li></a>
                  <a class="menu-item-link" href="{{route('users.index')}}"><li class="asside-menu-item">Użytkownicy</li></a>
                  <ul class="asside-submenu">
                      <a class="menu-item-link" href="{{route('roles.index')}}"><li class="asside-menu-item">Role</li></a>
                      <a class="menu-item-link" href="{{route('permissions.index')}}"><li class="asside-menu-item">Uprawnienia</li></a>
                  </ul>
                  </ul>
                  <a class="menu-item-link" href="#"><p class="asside-menu-item">Ustawienia</p></a>
                  </ul>
              </div>
            </div>
            <div class="col content m-r-20">
              @include('dashboard.includes.message')
              @yield('content')
            </div>
        </div>
    </div>

</body>

<!-- Scripts -->

<script src="{{ asset('js/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
@stack('scripts')

</html>
